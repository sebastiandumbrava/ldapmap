#!/usr/bin/python3

import ldap
import sys
import global_variables as gv
import argparse

def parse_arguments():
    description = "Windows domain enumeration with LDAP queries"
    parser = argparse.ArgumentParser(add_help = True, description=description)
    parser.add_argument('dc_ip', type=str, help="ip address") #, metavar='N', type=str, nargs='+', help='ip address')
    domain_arguments = parser.add_argument_group("Domain options")
    domain_arguments.add_argument('-d', '--domain', metavar='DOMAIN', dest='domain', type=str, help="Domain options")
    
    return parser.parse_args()

def default_naming_contexts_init(dc_ip):
    con = ldap.initialize('ldap://' + dc_ip)
    con.simple_bind_s('', '')
    objectFilter = '(objectclass=*)'
    attrs = ['domainFunctionality', 'forestFunctionality', 'domainControllerFunctionality']
    try:
        # rawFunctionality = self.do_ldap_query('', ldap.SCOPE_BASE, objectFilter, attrs)
        res = con.search_s('', ldap.SCOPE_BASE, "(objectclass=*)", attrs)
        functionality_levels = res[0][1]
    except Exception as e:
        print(e)

    return functionality_levels



def default_naming_context_init(dc_ip):
    defaultNamingContext = None
    try:
        con = ldap.initialize('ldap://' + dc_ip)
        con.simple_bind_s('', '')
        res = con.search_s('', ldap.SCOPE_BASE, '(objectClass=*)')
        rootDSE = res[0][1]
        
        defaultNamingContext = rootDSE['defaultNamingContext'][0]
    except Exception as e:
        print(e)

    return defaultNamingContext


def get_users(dc_ip, base_dn):
    attrs = ['cn', 'userPrincipalName']
    object_filter = 'objectCategory=user'
    res = None
    try:
        con = ldap.initialize("ldap://" + dc_ip)
        con.simple_bind_s('', '')
        res = con.search_s(base_dn, ldap.SCOPE_SUBTREE, object_filter, attrs)
    except Exception as e:
        print(e)
        pass
    
    if res:
        for key in res:
            if len(key) > 1:
                if type(key[1]) == dict:
                    if 'userPrincipalName' in key[1].keys():
                        print(key[1]['userPrincipalName'][0].decode())
                    
    return

def main():
    args = parse_arguments()
    dc_ip = args.dc_ip
    default_naming_contexts = default_naming_contexts_init(dc_ip) 
    defaultNamingContext = default_naming_context_init(dc_ip)
    base_dn = defaultNamingContext.decode()
    get_users(dc_ip, base_dn)    
    if defaultNamingContext:
        print(defaultNamingContext.decode())
    if default_naming_contexts:
        print(default_naming_contexts)
    return

if __name__ == "__main__":
    main()


